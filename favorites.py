# Welcome to the second Git exercise.
# We're in a Python file this time, but the rules are
# mostly the same.

# Let's see what's different:
first_question = "What is your favorite color?"

# in the string below:
first_question_answer = "Blue"

# After you've committed and pushed, repeat the same process for
# the rest of the questions!

second_question = "What is your favorite food?"
second_question_answer = "Tacos"

third_question = "Who is your favorite fictional character? ex. Mickey Mouse, Bugs Bunny"
third_question_answer = "batman"

"""
!! Reminder that you should be:
  - adding your changes after you make them
  - committing the changes you added
  - pushing your commits to your forked repository
  After answering each question!!
"""

fourth_question = "What is your favorite animal?"
fourth_question_answer = "tiger"

fifth_question = "What is your favorite programming language? (Hint: You can always say Python!!)"
fifth_question_answer = "python"
